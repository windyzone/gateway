/**
 * 
 */
package divpro;

import java.util.concurrent.TimeUnit;

/**
 * @Description:  
 * @version: v1.0.0
 * @author: wbl
 * @date: 2019年9月9日 下午5:25:23
 */
public class DivMultiprotocolSelection {

//国家电网
//	public static String PROTOCOL_TYPES = "376";
//	public static int HEARTBEATTIME = 3;
//	public static TimeUnit TIME_UNIT = TimeUnit.MINUTES;
	
//	public static String PROTOCOL_TYPES = "645";
//	public static int HEARTBEATTIME = 10;
//	public static TimeUnit TIME_UNIT = TimeUnit.SECONDS;
	
//	public static String PROTOCOL_TYPES = "698";
//	public static int HEARTBEATTIME = 60;
//	public static TimeUnit TIME_UNIT = TimeUnit.SECONDS;
	
//	public static String PROTOCOL_TYPES = "104";
//	public static int HEARTBEAT_TIME = 20;
//	public static TimeUnit TIME_UNIT = TimeUnit.SECONDS;
//二进制
//	public static String PROTOCOL_TYPES = "2";
//	public static int HEARTBEATTIME = 30;
//	public static TimeUnit TIME_UNIT = TimeUnit.MINUTES;
	
//充电桩
//	public static String PROTOCOL_TYPES = "AA55";//0xAA55或者0xA5A5，
//	public static int HEARTBEATTIME = 30;
//	public static TimeUnit TIME_UNIT = TimeUnit.MINUTES;
//MQTT
//	public static String PROTOCOL_TYPES = "MQTT";
//	public static int HEARTBEATTIME = 30;
//	public static TimeUnit TIME_UNIT = TimeUnit.MINUTES;	
	
//自定义心跳时间，用于本地效果测试
	public static String PROTOCOL_TYPES = "1";
	public static int HEARTBEATTIME = 20;
	public static TimeUnit TIME_UNIT = TimeUnit.SECONDS;
}
