/**
 * 
 */
package divpro;

import connect.ConfigDesign;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import jedis.Jedisclusters;

/**
 * @Description:  
 * @version: v1.0.0
 * @author: wbl
 * @date: 2019年8月30日 下午2:49:52
 */
public class DivServerHandler  extends ChannelInboundHandlerAdapter{
	 @Override
	    public void channelActive(ChannelHandlerContext ctx) throws Exception {
		 	ConfigDesign.CLIENT_IP_CONNECT.put(ctx.channel().remoteAddress().toString().substring(1),ctx.channel());
		 	System.out.println("收到客户端连接:" + ctx.channel().remoteAddress().toString());
//		 	ToClientDesign.jedisCluster.hset(ctx.channel().remoteAddress().toString().substring(1),"ONLINE","1");
	    }
	 
	  @Override
	    public void channelInactive(ChannelHandlerContext ctx) throws Exception {//ctx.close();//close会自动调用本方法
	        System.out.println("客户端断开链接:" + ctx.channel().remoteAddress().toString());
//		 	ToClientDesign.jedisCluster.hset(ctx.channel().remoteAddress().toString().substring(1),"ONLINE","0");
		 	ConfigDesign.CLIENT_IP_CONNECT.remove(ctx.channel().remoteAddress().toString().substring(1),ctx.channel());
	  }
		
	    @Override
	    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
	    	String body = (String)msg;
	        body = ctx.channel().remoteAddress().toString().substring(1) +"_"+body;
	        ctx.fireChannelRead(body);//转到下一个handler，body是下一个handler的msg
	    }
	    
	    /**
	     * 超时处理，如果HEARTBEAT_TIME秒没有收到客户端的心跳，就触发; 
	     */
	    /** 空闲次数 */
	    private int idle_count = 1;
	    @Override
	    public void userEventTriggered(ChannelHandlerContext ctx, Object obj) throws Exception {
	    	 if (obj instanceof IdleStateEvent) {
	             IdleStateEvent event = (IdleStateEvent) obj;
	             if (IdleState.READER_IDLE.equals(event.state())) { 
	                 if (idle_count > 2) {
	                     System.out.println("超过两次无客户端请求，关闭该channel");
	                     ctx.channel().close();
	                 }
	                 System.out.println("已等待"+DivMultiprotocolSelection.HEARTBEATTIME
	                		 +DivMultiprotocolSelection.TIME_UNIT
	                		 +"还没收到客户端发来的消息");
	                 idle_count++;
	             }
	         } else {
	             super.userEventTriggered(ctx, obj);
	         }
	    }
	 
	    /**
	     * 当发生异常时，打印异常 日志，释放客户端资源
	     */
	    @Override
	    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
	        /**释放资源*/
//	    	ToClientDesign.jedisCluster.hset(ctx.channel().remoteAddress().toString().substring(1),"ONLINE","0");
	        System.out.println("Unexpected exception from downstream : " + cause.getMessage());
	        ctx.close();
	    }
}
