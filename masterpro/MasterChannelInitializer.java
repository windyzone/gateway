/**
 * 
 */
package masterpro;

import java.util.concurrent.TimeUnit;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import protobuf.MasterMessage;

/**
 * @Description:  Filter
 * @version: v1.0.0
 * @author: wbl
 * @date: 2019年9月3日 下午5:01:10
 */
public class MasterChannelInitializer extends ChannelInitializer<SocketChannel>{
	int masterid = 0;
	
	public MasterChannelInitializer(int masterid){
		this.masterid = masterid;
	}

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new IdleStateHandler(0, 10, 0, TimeUnit.SECONDS));
		ch.pipeline().addLast("decoder",  new ProtobufDecoder(MasterMessage.Message.getDefaultInstance()));  
        ch.pipeline().addLast(new StringEncoder());//组码发给客户端
      	ch.pipeline().addLast(new MasterClientHandler(masterid));
	}

}
