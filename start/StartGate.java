package start;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import connect.PoolDesign;

/**
 * @Description:  
 * @version: v1.0.0
 * @author: wbl
 * @date: 2019年9月10日 下午2:19:20
 */

@SpringBootApplication
public class StartGate implements EmbeddedServletContainerCustomizer{
	public static void main(String[] args) {
		 SpringApplication.run(StartGate.class, args);
		 PoolDesign startpool = new PoolDesign();
		 startpool.start();
	}
	
	@Override
	public void customize(ConfigurableEmbeddedServletContainer arg0) {
		arg0.setPort(9990);
	}
}
