/**
 * 
 */
package connect;

import java.util.Date;
import javax.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import divpro.DivChannelInitializer;
import freeconfig.GetProperties;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import jedis.Jedisclusters;

/**
 * @Description:  server=gate
 * @version: v1.0.0
 * @author: wbl
 * @date: 2019年8月30日 上午10:04:44
 */
@Configuration
public class ToClientDesign implements Runnable{
	private int poolNum = 0;
	

	
	@Resource
	public static  Jedisclusters jedisCluster;
	
	public ToClientDesign(int poolnum){
		this.poolNum = poolnum;
	}
	
	@Override
	public void run() {
		//一旦 Channel 注册了，就处理该Channel对应的所有I/O 操作。
		EventLoopGroup bossGroup = new NioEventLoopGroup(ConfigDesign.BIZGROUPSIZE);
        EventLoopGroup workerGroup = new NioEventLoopGroup(ConfigDesign.BIZTHREADSIZE);
        try {
            /** 配置服务端的 NIO 线程池,用于网络事件处理，实质上他们就是 Reactor 线程组
             * bossGroup 用于服务端接受客户端连接，workerGroup 用于进行 SocketChannel 网络读写
             * */
            ServerBootstrap gateServer = new ServerBootstrap();
            gateServer.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, ConfigDesign.CACHESIZE)
                    .childHandler(new DivChannelInitializer());
 
            String port = GetProperties.getValue("client.port");
            System.out.println("read client config:"+port);
            ChannelFuture f = gateServer.bind(Integer.parseInt(port)).sync();
            System.out.println("bind: ["+new Date()+"]   服务器开始监听端口"+port+"，等待客户端连接.........");
            /**下面会进行阻塞,线程变为wait状态，等待服务器连接关闭
             * 服务器同步连接断开时,这句代码才会往下执行,
             * */
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            /**优雅退出，释放线程池资源*/
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
		
		
		
		
		
		
		
	}
	
	
	
	

}
