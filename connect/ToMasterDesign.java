/**
 * 
 */
package connect;

import java.util.Date;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import jedis.Jedisclusters;
import masterpro.MasterChannelInitializer;
import masterpro.MasterDisconnectListener;

/**
 * @Description:  client------>master
 * @version: v1.0.0
 * @author: wbl
 * @date: 2019年8月30日 上午10:04:30
 */
public class ToMasterDesign  implements Runnable{
	private int masterid = 0;
	private String ip = "";
	private int port = 0;
	
//	@Resource
//	public  Jedisclusters jedisCluster; //都调用的是ToClientDesign里的
	
	
	public ToMasterDesign(int masterid, String ip,int port){
		this.masterid = masterid;
		this.ip =ip;
		this.port = port;
	}
	
	
	@Override
	public void run() {
		connect();
		
	}
	
	public void connect(){
		
		 EventLoopGroup group = new NioEventLoopGroup();
         try {
             Bootstrap gate = new Bootstrap();
             gate.group(group)
     		 .channel(NioSocketChannel.class)
     		 .option(ChannelOption.TCP_NODELAY, true)
     		 .handler(new MasterChannelInitializer(masterid));
             
             ChannelFuture channelFuture = gate.connect(ip, port).sync();

             if(channelFuture.channel().isActive()){
            	sendMsg(channelFuture.channel());
             }
      
             //负责监听启动时连接失败，重新连接功能 ,也有人使用channelInactive，直接进行重连
             channelFuture.addListener(new MasterDisconnectListener(masterid,ip,port)); //添加监听，处理重连
             
             channelFuture.channel().closeFuture().sync();
             
             
         } catch (InterruptedException e) {
             e.printStackTrace();
         } finally {
             group.shutdownGracefully();
         }
	}
	
	
	
   public void sendMsg(Channel channel) {
	   String str = "gateway_Login";
	   System.out.println("Login： ["+new Date() + "]   "+channel.remoteAddress().toString().substring(1));
       channel.writeAndFlush(str);
    }

}
